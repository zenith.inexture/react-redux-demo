import React from "react";
import { connect } from "react-redux";
import { addice, buyice } from "./redux/ice-cream/iceAction";

function Icecontainer(props) {
  return (
    <div>
      <br></br>
      <p style={{ color: "blue" }}>Ice-cream Actions with connect methods</p>
      {props.numOfice === 0 ? (
        <h2 style={{ color: "red" }}>**Out Of Stock</h2>
      ) : (
        <h2>Number of Ice-cream - {props.numOfice}</h2>
      )}
      <button onClick={props.buyice}>Buy 2 Ice-creams</button>
      <button onClick={props.addice}>Add 10 Ice-cream</button>
      {console.log("ice container")}
    </div>
  );
}

//this function retrive the particular state property
const mapStateToProps2 = (state) => {
  return {
    numOfice: state.ice.numOfice,
  };
};
//this function used to retrive the dispatch method to connect with action creater
const mapDispatchToProps2 = (dispatch) => {
  return {
    buyice: () => dispatch(buyice()),
    addice: () => dispatch(addice()),
  };
};

export default connect(mapStateToProps2, mapDispatchToProps2)(Icecontainer);
