//logger middleware form redux logger
import logger from "redux-logger";

import thunk from "redux-thunk";

//we have to install devtools extension in chrome
import { composeWithDevTools } from "redux-devtools-extension";

import { createStore, combineReducers, applyMiddleware } from "redux";

//both reducer
import cakereducer from "./cake/cakeReducer";
import icereducer from "./ice-cream/iceReducer";
import userreducer from "./users/userReducer";
import cakepreducer from "./cakepayload/cakepReducer";

// combine the reducers
const rootreducer = combineReducers({
  cake: cakereducer,
  cakep: cakepreducer,
  ice: icereducer,
  user: userreducer,
});

//create the store and pass reducer and middleware
const store = createStore(
  rootreducer,
  // { cake: cakereducer },
  composeWithDevTools(applyMiddleware(logger, thunk))
);
//logger middleware log the all action perform by us

export default store;
