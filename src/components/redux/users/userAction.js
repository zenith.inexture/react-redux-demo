import axios from "axios";

import {
  FETCH_USERS_FAILURE,
  FETCH_USERS_REQUEST,
  FETCH_USERS_SUCCESS,
} from "./userTypes";

const fetchusersrequest = () => {
  return {
    type: FETCH_USERS_REQUEST,
  };
};
const fetchuserssuccess = (users) => {
  return {
    type: FETCH_USERS_SUCCESS,
    payload: users,
  };
};
const fetchusersfailure = (error) => {
  return {
    type: FETCH_USERS_FAILURE,
    payload: error,
  };
};

//this is async action creater function
export const fetchusers = () => {
  console.log("user action");
  return function (dispatch) {
    dispatch(fetchusersrequest());
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then((response) => {
        dispatch(fetchuserssuccess(response.data));
      })
      .catch((error) => {
        dispatch(fetchusersfailure(error.message));
      });
  };
};
