import { ADD_CAKE, BUY_CAKE } from "./cakeTypes";

//cake action - (we can write directly in dispath method)
export const buycake = () => {
  console.log("buy cake");
  return {
    type: BUY_CAKE,
  };
};
export const addcake = () => {
  console.log("add cake");
  return {
    type: ADD_CAKE,
  };
};
