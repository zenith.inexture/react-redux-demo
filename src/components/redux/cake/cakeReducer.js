import { ADD_CAKE, BUY_CAKE } from "./cakeTypes";

//initial value of cake
const initialStatecake = {
  numOfcake: 10,
};

//reducer function for cake
const cakereducer = (state = initialStatecake, action) => {
  console.log("cake reducer");
  switch (action.type) {
    case BUY_CAKE:
      if (state.numOfcake <= 0) {
        return {
          ...state,
          numOfcake: 0,
        };
      } else {
        return {
          ...state,
          numOfcake: state.numOfcake - 1,
        };
      }
    case ADD_CAKE:
      return {
        ...state,
        numOfcake: state.numOfcake + 5,
      };
    default:
      return state;
  }
};

export default cakereducer;
