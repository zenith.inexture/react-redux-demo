import { ADD_ICE, BUY_ICE } from "./iceTypes";

//ice-cream action
export const buyice = () => {
  console.log("buy ice");
  return {
    type: BUY_ICE,
  };
};
export const addice = () => {
  console.log("add ice");
  return {
    type: ADD_ICE,
  };
};
