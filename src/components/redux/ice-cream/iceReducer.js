import { ADD_ICE, BUY_ICE } from "./iceTypes";

const initialStateice = {
  numOfice: 10,
};

//reducer function for ice-cream
const icereducer = (state = initialStateice, action) => {
  console.log("ice reducer");
  switch (action.type) {
    case BUY_ICE:
      if (state.numOfice > 0) {
        return {
          ...state,
          numOfice: state.numOfice - 2,
        };
      } else {
        return {
          ...state,
          numOfice: 0,
        };
      }
    case ADD_ICE:
      return {
        ...state,
        numOfice: state.numOfice + 10,
      };
    default:
      return state;
  }
};

export default icereducer;
