import { ADD_CAKEP, BUY_CAKEP } from "./cakepTypes";

//initial value of cake
const initialStatecakep = {
  numOfcakep: 10,
};

//reducer function for cake
const cakepreducer = (state = initialStatecakep, action) => {
  console.log("payload reducer");
  switch (action.type) {
    case BUY_CAKEP:
      // add this condition when use r using the payload  (|| action.payload > state.numOfcake)
      if (state.numOfcakep <= 0 || action.payload > state.numOfcakep) {
        return {
          ...state,
          numOfcakep: 0,
        };
      } else {
        return {
          ...state,
          //when u r using the payload ->  numOfcakep: state.numOfcakep - action.payload,
          numOfcakep: state.numOfcakep - action.payload,
        };
      }
    case ADD_CAKEP:
      return {
        ...state,
        numOfcakep: state.numOfcakep + 5,
      };
    default:
      return state;
  }
};

export default cakepreducer;
