import { ADD_CAKEP, BUY_CAKEP } from "./cakepTypes";

//cake with payload action
export const buycakep = (number) => {
  console.log("buy cake payload");
  return {
    type: BUY_CAKEP,
    payload: number,
  };
};
export const addcakep = () => {
  console.log("add cake payload");
  return {
    type: ADD_CAKEP,
  };
};
