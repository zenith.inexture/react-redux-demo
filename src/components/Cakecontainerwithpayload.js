import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { addcakep, buycakep } from "./redux/cakepayload/cakepAction";

function Cakecontainerwithpayload() {
  const [number, setNumber] = React.useState(1);
  //with this hooks we don't have to write the mapStatetoprops function
  const numOfcakep = useSelector((state) => state.cakep.numOfcakep);

  //with this hooks we don't have to write the mapDispatchToProps function
  const dispatch = useDispatch();

  return (
    <div>
      <br></br>
      <p style={{ color: "blue" }}>
        Cake Actions with useSelector and useDispatch hooks and with payload
      </p>
      {numOfcakep === 0 ? (
        <h2 style={{ color: "red" }}>**Out Of Stock</h2>
      ) : (
        <h2>Number of Cakes - {numOfcakep}</h2>
      )}
      <label>
        Number of Cakes u want to buy :
        <input
          type="number"
          value={number}
          onChange={(e) => setNumber(e.target.value)}
        ></input>
      </label>
      {numOfcakep > 0 ? (
        <button onClick={() => dispatch(buycakep(number))}>
          Buy {number} Cake
        </button>
      ) : null}
      {numOfcakep > 0 ? (
        number > numOfcakep ? (
          <p style={{ color: "red" }}>You will get {numOfcakep} cakes</p>
        ) : (
          <p style={{ color: "red" }}>You will get {number} cakes</p>
        )
      ) : null}
      <button onClick={() => dispatch(addcakep())}>Add 5 Cakes</button>
      {console.log("cake payload container")}
    </div>
  );
}

export default Cakecontainerwithpayload;
