import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { addcake, buycake } from "./redux/cake/cakeAction";

function Cakecontainerwithhooks() {
  //with this hooks we don't have to write the mapStatetoprops function
  const numOfcake = useSelector((state) => state.cake.numOfcake);

  //with this hooks we don't have to write the mapDispatchToProps function
  const dispatch = useDispatch();

  return (
    <div>
      <br></br>
      <p style={{ color: "blue" }}>
        Cake Actions with useSelector and useDispatch hooks
      </p>
      {numOfcake === 0 ? (
        <h2 style={{ color: "red" }}>**Out Of Stock</h2>
      ) : (
        <h2>Number of Cakes - {numOfcake}</h2>
      )}
      <button onClick={() => dispatch(buycake())}>Buy Cake</button>
      <button onClick={() => dispatch(addcake())}>Add 5 Cakes</button>
      {console.log("cake hook container")}
    </div>
  );
}

export default Cakecontainerwithhooks;
