import React from "react";
import { connect } from "react-redux";
import { fetchusers } from "./redux/users/userAction";

function Usercontainer({ userData, fetchusers }) {
  React.useEffect(() => {
    fetchusers();
  }, []);

  return (
    <>
      <br></br>
      <p style={{ color: "blue" }}>
        Async Actions with connect method to fetch data
      </p>
      {userData.loading ? (
        <h2 style={{ color: "green" }}>Loading...</h2>
      ) : userData.error ? (
        <h2>{userData.error}</h2>
      ) : (
        <div>
          <h2>Users Name</h2>
          {userData.users.map((users, index) => (
            <p key={index}>{users.name}</p>
          ))}
        </div>
      )}
      {console.log("user container")}
    </>
  );
}

const mapStateToProps3 = (state) => {
  return {
    userData: state.user,
  };
};

const mapDispatchToProps3 = (dispatch) => {
  return {
    fetchusers: () => dispatch(fetchusers()),
  };
};

export default connect(mapStateToProps3, mapDispatchToProps3)(Usercontainer);
