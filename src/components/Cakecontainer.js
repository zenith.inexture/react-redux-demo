import React from "react";
import { connect } from "react-redux";
import { addcake, buycake } from "./redux/cake/cakeAction";

function Cakecontainer(props) {
  return (
    <div>
      <p style={{ color: "blue" }}>Cake Actions with connect method</p>
      {props.numOfcake === 0 ? (
        <h2 style={{ color: "red" }}>**Out Of Stock</h2>
      ) : (
        <h2>Number of Cakes - {props.numOfcake}</h2>
      )}
      <button onClick={props.buycake}>Buy Cake</button>
      <button onClick={props.addcake}>Add 5 Cakes</button>
      {console.log("cake container")}
    </div>
  );
}

//this function retrive the particular state property
const mapStateToProps1 = (state) => {
  return {
    numOfcake: state.cake.numOfcake,
  };
};
//this function used to retrive the dispatch method to connect with action creater
const mapDispatchToProps1 = (dispatch) => {
  return {
    buycake: () => dispatch(buycake()),
    addcake: () => dispatch(addcake()),
  };
};

export default connect(mapStateToProps1, mapDispatchToProps1)(Cakecontainer);
