import "./App.css";
import { Provider } from "react-redux";
import store from "./components/redux/store";
import Cakecontainer from "./components/Cakecontainer";
import Icecontainer from "./components/Icecontainer";
import Cakecontainerwithhooks from "./components/Cakecontainerwithhooks";
import Cakecontainerwithpayload from "./components/Cakecontainerwithpayload";
import Usercontainer from "./components/Usercontainer";

// Reducers must be pure functions. Given any input, output must always be the same.
// The state of your whole application is stored in an object tree within a single store.
// State is read-only. The only way to change the state is to emit an action, an object describing what happened.
// Changes in state are made with pure functions ( reducers).

function App() {
  return (
    //provider used to provide the store to all child components
    <Provider store={store}>
      <div className="App">
        <Cakecontainer />
        <Cakecontainerwithhooks />
        <Cakecontainerwithpayload />
        <Icecontainer />
        <Usercontainer />
      </div>
    </Provider>
  );
}

export default App;
